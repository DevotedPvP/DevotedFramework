package rip.devotedpvp.api.framework.player;

import rip.devotedpvp.api.framework.event.PlayerDataReceivedEvent;
import rip.devotedpvp.api.framework.DevotedNetwork;
import rip.devotedpvp.api.global.packets.messaging.messages.response.PlayerDataResponse;
import rip.devotedpvp.api.global.data.PlayerData;
import rip.devotedpvp.api.global.data.PunishmentData;
import rip.devotedpvp.api.global.player.DevotedPlayer;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.IOException;
import java.util.UUID;
import java.util.logging.Level;


public class BukkitDevotedPlayer extends DevotedPlayer<Player> {
    public static BukkitDevotedPlayer getObject(UUID u) {
        return (BukkitDevotedPlayer) getPlayerObjectMap().get(u);
    }

    public BukkitDevotedPlayer(UUID u, PlayerData data) {
        super(u, data.getRaw());
    }

    public String getName() {
        return getPlayer().getName();
    }

    public boolean isOnline(){
        return getPlayer() != null && getPlayer().isOnline();
    }

    public void save() {
        throw new UnsupportedOperationException("Server-side cannot save");
    }

    protected void update() {
        try {
            DevotedNetwork.getInstance().getPacketHub().sendMessage(DevotedNetwork.getInstance().getProxy(), new PlayerDataResponse(getName(), getData().getRaw()));
            new BukkitRunnable(){
                public void run() {
                    //Doesn't change before & after but nothing can be done
                    Bukkit.getServer().getPluginManager().callEvent(new PlayerDataReceivedEvent(getPlayer(), getData()));
                }
            }.runTaskAsynchronously(DevotedNetwork.getInstance().getPlugin());
        } catch (IOException e) {
            DevotedNetwork.getInstance().getLogger().log(Level.WARNING, "Failed to send data update: ", e);
        }
    }
}
