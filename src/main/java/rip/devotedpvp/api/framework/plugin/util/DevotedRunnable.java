package rip.devotedpvp.api.framework.plugin.util;

import rip.devotedpvp.api.framework.DevotedNetwork;
import rip.devotedpvp.api.framework.plugin.DevotedAddon;
import org.bukkit.scheduler.BukkitTask;

import java.util.concurrent.TimeUnit;

public abstract class DevotedRunnable implements Runnable {
    private BukkitTask task = null;

    private void taskCheck() {
        if (task != null) {
            throw new IllegalArgumentException("Already running");
        }
    }

    public BukkitTask runTask(DevotedAddon plugin) {
        taskCheck();
        return task = DevotedNetwork.getInstance().registerRunnable(plugin, this, TimeUnit.MILLISECONDS, 0L, false, false);
    }

    public BukkitTask runTaskAsynchonrously(DevotedAddon plugin) {
        taskCheck();
        return task = DevotedNetwork.getInstance().registerRunnable(plugin, this, TimeUnit.MILLISECONDS, 0L, false, true);
    }

    public BukkitTask runTaskLater(DevotedAddon plugin, TimeUnit unit, long time) {
        taskCheck();
        return task = DevotedNetwork.getInstance().registerRunnable(plugin, this, unit, time, false, false);
    }

    public BukkitTask runTaskLaterAsynchronously(DevotedAddon plugin, TimeUnit unit, long time) {
        taskCheck();
        return task = DevotedNetwork.getInstance().registerRunnable(plugin, this, unit, time, false, true);
    }

    public BukkitTask runTaskTimer(DevotedAddon plugin, TimeUnit unit, long time) {
        taskCheck();
        return task = DevotedNetwork.getInstance().registerRunnable(plugin, this, unit, time, true, false);
    }

    public BukkitTask runTaskTimerAsynchronously(DevotedAddon plugin, TimeUnit unit, long time) {
        taskCheck();
        return task = DevotedNetwork.getInstance().registerRunnable(plugin, this, unit, time, true, true);
    }

    public BukkitTask getTask() {
        return task;
    }

    public void cancel() {
        task.cancel();
    }
}
