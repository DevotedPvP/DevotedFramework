package rip.devotedpvp.api.framework.plugin;

import rip.devotedpvp.api.framework.DevotedNetwork;
import rip.devotedpvp.api.framework.DevotedPlugin;
import rip.devotedpvp.api.framework.plugin.loader.AddonDescriptionFile;
import rip.devotedpvp.api.framework.plugin.loader.DevotedAddonLoader;
import rip.devotedpvp.api.global.packets.messaging.messages.request.ServerShutdownRequest;
import rip.devotedpvp.api.global.file.DownloadUtil;
import de.mickare.xserver.net.XServer;
import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.event.Listener;
import org.bukkit.scheduler.BukkitTask;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;


public abstract class DevotedAddon {
    private AddonDescriptionFile descriptionFile;
    private File file;
    private DevotedAddonLoader loader;
    
    public org.bukkit.plugin.PluginDescriptionFile getDescription() {
        return AddonDescriptionFile.asMirror(descriptionFile);
    }

    public AddonDescriptionFile getDescriptionBubble() {
        return descriptionFile;
    }

    public boolean isEnabled() {
        return getPlugin().isEnabled();
    }

    public void __init__(DevotedAddonLoader loader) {
        this.loader = loader;
        file = loader.getJar();
        descriptionFile = loader.getFile();
    }

    public DevotedAddonLoader getLoader() {
        return loader;
    }

    public void onLoad() {
    }


    public void onEnable() {

    }

    public void onDisable() {

    }

    public DevotedPlugin getPlugin() {
        return DevotedNetwork.getInstance().getPlugin();
    }

    public abstract long finishUp();

    public File getDataFolder() {
        return new File(getPlugin().getDataFolder() + File.separator + getName());
    }

    public Server getServer() {
        return getPlugin().getServer();
    }

    public String getName() {
        return descriptionFile.getName();
    }

    public BukkitTask runTask(Runnable r) {
        return DevotedNetwork.getInstance().registerRunnable(this, r, TimeUnit.MILLISECONDS, 0L, false, false);
    }

    public BukkitTask runTaskAsynchonrously(Runnable r) {
        return DevotedNetwork.getInstance().registerRunnable(this, r, TimeUnit.MILLISECONDS, 0L, false, true);
    }

    public BukkitTask runTaskLater(Runnable r, TimeUnit unit, long time) {
        return DevotedNetwork.getInstance().registerRunnable(this, r, unit, time, false, false);
    }

    public BukkitTask runTaskLaterAsynchronously(Runnable r, TimeUnit unit, long time) {
        return DevotedNetwork.getInstance().registerRunnable(this, r, unit, time, false, true);
    }

    public BukkitTask runTaskTimer(Runnable r, TimeUnit unit, long time) {
        return DevotedNetwork.getInstance().registerRunnable(this, r, unit, time, true, false);
    }

    public BukkitTask runTaskTimerAsynchronously(Runnable r, TimeUnit unit, long time) {
        return DevotedNetwork.getInstance().registerRunnable(this, r, unit, time, true, true);
    }

    public void registerListener(Listener l) {
        DevotedNetwork.getInstance().registerListener(this, l);
    }

    public String getArtifact() {
        return getName();
    }

    public File getReplace() {
        return file;
    }

    public void updateTaskAfter() {
        XServer proxy = DevotedNetwork.getInstance().getProxy();
        try {
            DevotedNetwork.getInstance().getPacketHub().sendMessage(proxy, new ServerShutdownRequest());
        } catch (IOException e) {
            DevotedNetwork.getInstance().getLogger().log(Level.WARNING, "Could not disconnect from proxy", e);
            Bukkit.shutdown();
            return;
        }
        try {
            proxy.disconnect();
        } catch (Exception e) {
            DevotedNetwork.getInstance().getLogger().log(Level.WARNING, "Could not disconnect from proxy", e);
            Bukkit.shutdown();
            return;
        }
        try {
            proxy.connect();
        } catch (Exception e) {
            DevotedNetwork.getInstance().getLogger().log(Level.WARNING, "Could not connect to proxy", e);
            Bukkit.shutdown();
        }
    }

    public void updateTaskBefore() {
        DevotedNetwork network = DevotedNetwork.getInstance();
        network.disableAddon();
    }

    public void downloadFile(File to, String name) throws Exception {
        DownloadUtil.download(to, name, DevotedNetwork.getInstance().getFileConnection());
    }
}
