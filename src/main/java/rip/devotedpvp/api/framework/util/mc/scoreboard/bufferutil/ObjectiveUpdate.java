package rip.devotedpvp.api.framework.util.mc.scoreboard.bufferutil;

import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;


public abstract class ObjectiveUpdate {
    private DisplaySlot slot;

    public ObjectiveUpdate(DisplaySlot slot) {
        this.slot = slot;
    }

    public DisplaySlot getSlot() {
        return slot;
    }

    public abstract void update(Objective o);
}
