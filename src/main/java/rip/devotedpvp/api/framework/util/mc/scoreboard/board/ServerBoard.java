package rip.devotedpvp.api.framework.util.mc.scoreboard.board;

import rip.devotedpvp.api.framework.util.mc.scoreboard.api.BoardAPI;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public class ServerBoard extends BoardAPI {

    public ServerBoard(Map<DisplaySlot, String> displaynames) {
        super("ServerBoard", displaynames);
    }

    @Override
    public Collection<Player> getPlayers() {
        List<Player> online = new ArrayList<>();
        online.addAll(Bukkit.getOnlinePlayers());
        return online;
    }
}
