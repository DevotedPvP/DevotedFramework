package rip.devotedpvp.api.framework.util.mc.scoreboard.board;

import rip.devotedpvp.api.framework.util.mc.scoreboard.api.BoardAPI;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;

public class SingleBoard extends BoardAPI {
    private Player p;


    public SingleBoard(Player p, Map<DisplaySlot, String> displaynames) {
        super(p.getName(), displaynames);
        this.p = p;
    }

    @Override
    public Collection<Player> getPlayers() {
        return Collections.singletonList(p);
    }
}
