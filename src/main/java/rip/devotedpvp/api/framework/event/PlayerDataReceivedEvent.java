package rip.devotedpvp.api.framework.event;

import rip.devotedpvp.api.framework.player.BukkitDevotedPlayer;
import rip.devotedpvp.api.global.data.PlayerData;
import rip.devotedpvp.api.global.data.PunishmentData;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;


public class PlayerDataReceivedEvent extends Event{
    private static final HandlerList handlers = new HandlerList();

    private Player player;
    private PlayerData data;
    private PlayerData before;

    public PlayerDataReceivedEvent(Player player, PlayerData data) {
        super(true);
        this.player = player;
        this.data = data;
        this.before = BukkitDevotedPlayer.getObject(player.getUniqueId()).getData();
    }

    public Player getPlayer() {
        return player;
    }

    public PlayerData getData() {
        return data;
    }

    public BukkitDevotedPlayer getAfter(){
        return new BukkitDevotedPlayer(getPlayer().getUniqueId(), getData());
    }

    public BukkitDevotedPlayer getBefore(){
        //Make sure this cannot be modified
        return new BukkitDevotedPlayer(getPlayer().getUniqueId(), new PlayerData(before.getRaw()));
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}
