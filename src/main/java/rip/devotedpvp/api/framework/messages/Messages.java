package rip.devotedpvp.api.framework.messages;


import javax.annotation.Nullable;
import rip.devotedpvp.api.framework.DevotedNetwork;
import rip.devotedpvp.api.framework.messages.titlemanager.ActionType;
import rip.devotedpvp.api.framework.messages.titlemanager.NMSTitles;
import rip.devotedpvp.api.framework.messages.titlemanager.types.SubtitleTitle;
import rip.devotedpvp.api.framework.messages.titlemanager.types.TimingTicks;
import rip.devotedpvp.api.framework.messages.titlemanager.types.TimingTitle;
import rip.devotedpvp.api.framework.messages.titlemanager.types.TitleTitle;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.logging.Level;


public class Messages {

    public static void broadcastMessageAction(String message) {
        sendMessageAction(Bukkit.getOnlinePlayers(), message);
    }

    public static void sendMessageAction(Player[] ps, String message) {
        sendMessageAction(Arrays.asList(ps), message);
    }

    public static void sendMessageAction(Iterable<? extends Player> ps, String message) {
        for (Player p : ps) {
            sendMessageAction(p, message);
        }
    }

    public static void sendMessageAction(Player p, String message) {
        NMSTitles.sendChat(p, ActionType.ACTION, message);
    }

    public static void broadcastMessageTitle(@Nullable String title,@Nullable String subtitle,@Nullable TimingTicks timing) {
        sendMessageTitle(Bukkit.getOnlinePlayers(), title, subtitle, timing);
    }

    public static void sendMessageTitle(Player[] ps, String title,@Nullable String subtitle,@Nullable TimingTicks timing) {
        sendMessageTitle(Arrays.asList(ps), title, subtitle, timing);
    }

    public static void sendMessageTitle(Iterable<? extends Player> ps,@Nullable String title,@Nullable String subtitle,@Nullable TimingTicks timing) {
        for (Player p : ps) {
            sendMessageTitle(p, title, subtitle, timing);
        }
    }

    public static void sendMessageTitle(Player p,@Nullable String title, @Nullable String subtitle,@Nullable TimingTicks timing) {
        try {
            if(title != null) {
                NMSTitles.sendTitle(p, new TitleTitle(title));
            }
            if (subtitle != null) {
                NMSTitles.sendTitle(p,new SubtitleTitle(subtitle));
            }
            if(timing != null){
                NMSTitles.sendTitle(p,new TimingTitle(timing));
            }
        } catch (Throwable throwable) {
            DevotedNetwork.getInstance().getLogger().log(Level.WARNING, "Could not send title/subtitle",throwable);
        }
    }


}
