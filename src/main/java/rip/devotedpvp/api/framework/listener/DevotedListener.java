package rip.devotedpvp.api.framework.listener;

import rip.devotedpvp.api.framework.DevotedNetwork;
import rip.devotedpvp.api.framework.player.BukkitDevotedPlayer;
import rip.devotedpvp.api.framework.interaction.DataRequestTask;
import rip.devotedpvp.api.framework.util.mc.menu.Menu;
import rip.devotedpvp.api.global.packets.messaging.messages.handshake.PlayerCountUpdate;
import rip.devotedpvp.api.global.data.PlayerData;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.weather.WeatherChangeEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.potion.PotionEffect;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;


public class DevotedListener implements Listener {
    private DevotedNetwork network;

    private Map<String, Map<String, String>> data = new HashMap<>();

    public DevotedListener(DevotedNetwork network) {
        this.network = network;
    }

    protected DevotedNetwork getNetwork() {
        return network;
    }

    public Map<String, Map<String, String>> getData() {
        return data;
    }

    @EventHandler
    public void onRainSnow(WeatherChangeEvent e) {
        if (e.toWeatherState()) {
            e.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPreJoin(AsyncPlayerPreLoginEvent e) {
        data.put(e.getName(), DataRequestTask.requestAsync(e.getName()));
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerQuitPlayecount(PlayerQuitEvent e) {
        try {
            getNetwork().getPacketHub().sendMessage(getNetwork().getProxy(), new PlayerCountUpdate(Bukkit.getOnlinePlayers().size() - 1));
        } catch (IOException e1) {
            getNetwork().getLogger().log(Level.WARNING, "Could not send playercount update", e1);
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerQuitRemoval(PlayerQuitEvent e) {
        BukkitDevotedPlayer.getPlayerObjectMap().remove(e.getPlayer().getUniqueId());
        e.setQuitMessage(null);
    }

    @EventHandler
    public void onClick(InventoryClickEvent e) {
        Player player = (Player) e.getWhoClicked();
        Inventory inv = e.getInventory();
        if(inv != null && e.getClickedInventory() != null) {
            for (Menu menu : DevotedNetwork.getInstance().listMenu()) {
                if (menu.getInventory().equals(inv)) {
                    e.setCancelled(true);
                    if (e.getClickedInventory().equals(inv)) {
                        menu.click(player, e.getClick(), e.getSlot(), e.getCurrentItem());
                    }
                }
            }
        }
    }

    @EventHandler
    public void onPlayerPreproccessCommand(PlayerCommandPreprocessEvent e){
        BukkitDevotedPlayer player = BukkitDevotedPlayer.getObject(e.getPlayer().getUniqueId());
        if(!player.isAuthorized("ingame.bypass")){
            e.getPlayer().sendMessage(ChatColor.DARK_AQUA + "(!) Could not find a command with that name");
            e.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerJoin(PlayerJoinEvent e) {
        e.setJoinMessage(null);
        Player p = e.getPlayer();
        if(!data.containsKey(p.getName())){
            p.kickPlayer("Server timed out");
            throw new IllegalArgumentException("Player not found");
        }
        for (PotionEffect effect : p.getActivePotionEffects()) {
            p.removePotionEffect(effect.getType());
        }

        BukkitDevotedPlayer player = new BukkitDevotedPlayer(p.getUniqueId(), new PlayerData(data.remove(p.getName())));
        BukkitDevotedPlayer.getPlayerObjectMap().put(p.getUniqueId(), player);

        try {
            getNetwork().getPacketHub().sendMessage(getNetwork().getProxy(), new PlayerCountUpdate(Bukkit.getOnlinePlayers().size()));
        } catch (IOException e1) {
            getNetwork().getLogger().log(Level.INFO, "Could not send playercount update", e1);
        }
    }

}
