package rip.devotedpvp.api.framework.interaction;

import rip.devotedpvp.api.framework.DevotedNetwork;
import rip.devotedpvp.api.global.packets.messaging.messages.request.PlayerDataRequest;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.logging.Level;


public class DataRequestTask extends BukkitRunnable {
    public static void setData(String name, Map<String, String> received) {
        if (taskMap.containsKey(name.toLowerCase())) {
            taskMap.remove(name.toLowerCase()).setResult(received);
            DevotedNetwork.getInstance().getLogger().log(Level.INFO, "Received pending data for " + name);
        } else {
            DevotedNetwork.getInstance().getLogger().log(Level.WARNING, "Set data for invalid player " + name);
        }
    }

    public static Map<String, String> requestAsync(String name) {
        DevotedNetwork.getInstance().getLogger().log(Level.INFO, "Requesting data for " + name);
        DataRequestTask task = new DataRequestTask(name);
        taskMap.put(name.toLowerCase(), task);
        task.runTask(DevotedNetwork.getInstance().getPlugin());
        while (taskMap.containsKey(name.toLowerCase())) {

        }
        DevotedNetwork.getInstance().getLogger().log(Level.INFO, "Found data for " + name);
        return task.getResult();
    }

    private static ConcurrentMap<String, DataRequestTask> taskMap = new ConcurrentHashMap<>();
    private String name;
    private Map<String, String> result = null;

    private DataRequestTask(String name) {
        this.name = name;
    }

    public void run() {
        try {
            DevotedNetwork.getInstance().getPacketHub().sendMessage(DevotedNetwork.getInstance().getProxy(), new PlayerDataRequest(getName()));
        } catch (IOException e) {
            result = new HashMap<>();
        }
    }

    public Map<String, String> getResult() {
        return result;
    }

    public void setResult(Map<String, String> result) {
        this.result = result;
    }

    public String getName() {
        return name;
    }
}
