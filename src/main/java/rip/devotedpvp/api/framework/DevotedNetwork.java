package rip.devotedpvp.api.framework;

import rip.devotedpvp.api.framework.event.PlayerDataReceivedEvent;
import rip.devotedpvp.api.framework.event.ServerListUpdateEvent;
import rip.devotedpvp.api.framework.interaction.DataRequestTask;
import rip.devotedpvp.api.framework.listener.DevotedListener;
import rip.devotedpvp.api.framework.player.BukkitDevotedPlayer;
import rip.devotedpvp.api.framework.plugin.loader.AddonDescriptionFile;
import rip.devotedpvp.api.framework.plugin.DevotedAddon;
import rip.devotedpvp.api.framework.plugin.loader.DevotedAddonLoader;
import rip.devotedpvp.api.framework.plugin.manage.BukkitPlugman;
import rip.devotedpvp.api.framework.util.mc.items.EnchantGlow;
import rip.devotedpvp.api.framework.util.mc.menu.Menu;
import rip.devotedpvp.api.global.packets.PacketInfo;
import rip.devotedpvp.api.global.packets.PacketListener;
import rip.devotedpvp.api.global.packets.messaging.IPluginMessage;
import rip.devotedpvp.api.global.packets.messaging.messages.handshake.AssignMessage;
import rip.devotedpvp.api.global.packets.messaging.messages.handshake.RankDataUpdate;
import rip.devotedpvp.api.global.packets.messaging.messages.request.PlayerDataRequest;
import rip.devotedpvp.api.global.packets.messaging.messages.request.PlayerMoveRequest;
import rip.devotedpvp.api.global.packets.messaging.messages.request.PlayerMoveTypeRequest;
import rip.devotedpvp.api.global.packets.messaging.messages.request.ServerShutdownRequest;
import rip.devotedpvp.api.global.packets.messaging.messages.response.PlayerDataResponse;
import rip.devotedpvp.api.global.packets.messaging.messages.response.ServerListResponse;
import rip.devotedpvp.api.global.data.PlayerData;
import rip.devotedpvp.api.global.file.DownloadUtil;
import rip.devotedpvp.api.global.file.PropertiesFile;
import rip.devotedpvp.api.global.player.DevotedPlayer;
import rip.devotedpvp.api.global.plugin.DevotedAPI;
import rip.devotedpvp.api.global.ranks.Rank;
import rip.devotedpvp.api.global.type.ServerType;
import de.mickare.xserver.XServerPlugin;
import de.mickare.xserver.net.XServer;
import net.citizensnpcs.api.CitizensAPI;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.plugin.InvalidDescriptionException;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitScheduler;
import org.bukkit.scheduler.BukkitTask;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;


public class DevotedNetwork extends DevotedAPI<JavaPlugin> implements PacketListener {
    private static final Random random = new Random();
    private static final int VERSION = 1;

    public static String getChatFormat() {
        return chatFormat;
    }

    public static void setChatFormat(String chatFormat) {
        DevotedNetwork.chatFormat = chatFormat;
    }

    public static String getPrefix() {
        return prefix;
    }

    public static Random getRandom() {
        return random;
    }

    public static DevotedNetwork getInstance() {
        return instance;
    }

    private static DevotedNetwork instance;
    private static String prefix = ChatColor.DARK_GRAY + "[" + ChatColor.DARK_RED + ChatColor.BOLD.toString() + "Devoted" +
            ChatColor.GOLD + "PvP" +  ChatColor.DARK_GRAY + "] " + ChatColor.RESET;
    private static String chatFormat = "{prefix}{name}{suffix}{message}";
    public int FINALID;
    private ServerType type;
    private int id;
    private DevotedPlugin plugin;
    private XServer proxy;
    private DevotedAddon assigned;
    private DevotedListener listener = new DevotedListener(this);
    private BukkitPlugman plugman;
    private Set<Listener> listeners = new HashSet<>();
    private Set<BukkitTask> executed = new HashSet<>();
    private Set<Menu> registeredMenus = new HashSet<>();
    private File file;

    public DevotedNetwork(DevotedPlugin plugin) {
        super();
        instance = this;
        this.plugin = plugin;
    }

    public void registerListener(DevotedAddon plugin, Listener listener) {
        if (getAssigned() == plugin) {
            if (listeners.contains(listener)) {
                throw new IllegalArgumentException("Listener already registered");
            }
            listeners.add(listener);
            getPlugin().getServer().getPluginManager().registerEvents(listener, getPlugin());
        } else {
            throw new IllegalArgumentException("Plugin not registered");
        }
    }

    public void registerMenu(DevotedAddon addon, Menu menu) {
        if (getAssigned() == addon) {
            if (registeredMenus.contains(menu)) {
                throw new IllegalArgumentException("Menu already registered");
            }
            registeredMenus.add(menu);
        } else {
            throw new IllegalArgumentException("Plugin not registered");
        }
    }

    public void unregisterMenu(Menu menu) {
        if (registeredMenus.contains(menu)) {
            registeredMenus.remove(menu);
        } else {
            throw new IllegalArgumentException("Menu not registered");
        }
    }

    public void unregisterMenus() {
        registeredMenus.clear();
    }

    public BukkitTask registerRunnable(DevotedAddon plugin, Runnable r, TimeUnit unit, long time, boolean timer, boolean async) {
        if (getAssigned() == plugin) {
            long ticks = unit.toMillis(time) / 50;
            BukkitScheduler scheduler = getPlugin().getServer().getScheduler();
            BukkitTask task;
            if (async) {
                if (timer) {
                    task = scheduler.runTaskTimerAsynchronously(getPlugin(), r, ticks, ticks);
                } else {
                    task = scheduler.runTaskLater(getPlugin(), r, ticks);
                }
            } else if (timer) {
                task = scheduler.runTaskTimer(getPlugin(), r, ticks, ticks);
            } else {
                task = scheduler.runTaskLater(getPlugin(), r, ticks);
            }
            executed.add(task);
            return task;
        } else {
            throw new IllegalArgumentException("Plugin not registered");
        }
    }

    private void unregisterTasks() {
        for (BukkitTask task : executed) {
            try {
                task.cancel();
            } catch (Throwable throwable) {
            }
        }
        executed.clear();
    }

    private void unregisterListener() {
        for (Listener listener : listeners) {
            HandlerList.unregisterAll(listener);
        }
        listeners.clear();
    }

    public Set<Menu> listMenu() {
        return new HashSet<>(registeredMenus);
    }

    public void onDevotedEnable() {
        getLogger().log(Level.INFO, "Loading components");

        file = getPlugin().getFile();
        registerListener(getListener());
        EnchantGlow.getGlow();
        plugman = new BukkitPlugman(getPlugin().getServer());
        getPacketHub().registerListener(this);
    }

    public void onDevotedLoad() {
        getLogger().log(Level.INFO, "Loading the plugin...");

        if (!getPlugin().getDataFolder().exists()) {
            getLogger().log(Level.INFO, "Creating addon folder");
            getPlugin().getDataFolder().mkdir();
        }

        getLogger().log(Level.INFO, "Finding addons...");
    }

    public void onDevotedDisable() {
        if (getAssigned() != null) {
            disableAddon();
        }
        EnchantGlow.kill();
        try {
            getPacketHub().sendMessage(getProxy(), new ServerShutdownRequest());
        } catch (IOException e) {
            getLogger().log(Level.WARNING, "Could not send shutdown request", e);
        }
        CitizensAPI.getNPCRegistry().deregisterAll();
    }

    private void registerListener(Listener l) {
        getPlugin().getServer().getPluginManager().registerEvents(l, getPlugin());
    }

    public DevotedPlugin getPlugin() {
        return plugin;
    }

    public DevotedListener getListener() {
        return listener;
    }

    public void saveXServerDefaults() {
        int port;
        String ip;

        getLogger().log(Level.INFO, "Finding default server properties...");

        try {
            PropertiesFile thisserver = new PropertiesFile(new File("server.properties"));
            port = thisserver.getNumber("server-port").intValue();
            ip = (ip = thisserver.getString("server-ip")).equals("") ? "localhost" : ip;
            //this.FINALID = thisserver.getNumber("serverid").intValue();
        } catch (Exception e) {
            getLogger().log(Level.SEVERE, "Could not load this servers properties", e);
            endSetup("Could not load this servers properties");
            return;
        }

        getLogger().log(Level.INFO, "Found default server properties");


        //update the server id to match the port
        try {
            PropertiesFile.update(new File("network.properties"), "server-id", String.valueOf(port - 10000));
        } catch (Exception e) {
            getLogger().log(Level.SEVERE, "Could not update Network Properties", e);
            endSetup("Could not update Network Properties");
        }

        //load network properties
        try {
            this.FINALID = getNetworkProperties().getNumber("server-id").intValue();
            //TODO: implement method for setting specific server type
        } catch (Exception e) {
            getLogger().log(Level.SEVERE, "Could not load this servers network properties", e);
            endSetup("Could not load this servers network properties");
            return;
        }

        //this.FINALID = port - 10000;

        getLogger().log(Level.INFO, "Creating xserver files");

        File xserverfolder = new File("plugins" + File.separator + "XServer");

        if (!xserverfolder.exists()) {
            xserverfolder.mkdir();
        }

        File config = new File(xserverfolder + File.separator + "config.yml");

        if (!config.exists()) {
            try {
                config.createNewFile();
            } catch (IOException e) {
                getLogger().log(Level.WARNING, "Could not create XServer configuration", e);
                endSetup("Could not create xserver configuration");
            }
        }

        getLogger().log(Level.INFO, "Loading XServer configuration...");

        YamlConfiguration xserverconfig = YamlConfiguration.loadConfiguration(config);
        xserverconfig.set("useMotdForServername", false);
        xserverconfig.set("servername", String.valueOf(FINALID));
        xserverconfig.set("mysql.User", getConnection().getUser());
        xserverconfig.set("mysql.Pass", getConnection().getPassword());
        xserverconfig.set("mysql.Data", getConnection().getDatabase());
        xserverconfig.set("mysql.Host", getConnection().getHostname());
        xserverconfig.set("mysql.Port", getConnection().getPort());
        xserverconfig.set("mysql.TableXServers", "xserver_servers");
        xserverconfig.set("mysql.TableXGroups", "xserver_groups");
        xserverconfig.set("mysql.TableXServersGroups", "xserver_servergroups");

        getLogger().log(Level.INFO, "Saving XServer configuration...");

        try {
            xserverconfig.save(config);
        } catch (IOException e) {
            getLogger().log(Level.WARNING, "Could not save xserver files", e);
            endSetup("Could not save xserver files");
        }

        getLogger().log(Level.INFO, "Saved XServer configuration");
    }

    public Player getPlayer(UUID uuid) {
        return getPlugin().getServer().getPlayer(uuid);
    }

    public void stop() {
        getPlugin().getServer().shutdown();
    }

    public Logger getLogger() {
        if (getPlugin() != null) {
            return getPlugin().getLogger();
        }
        return Logger.getLogger("Minecraft","MicroNetwork");
    }

    public void disableAddon() {
        if (getAssigned() == null) {
            throw new IllegalArgumentException("No addon found");
        }
        getLogger().log(Level.INFO, "Disabling addon : " + getAssigned().getName());
        try {
            getAssigned().getLoader().close();
        } catch (Exception e) {
            getPlugin().getLogger().log(Level.WARNING, "Error while disabling plugin ", e);
        }
        try{
            getAssigned().onDisable();
        } catch (Exception e) {
            getPlugin().getLogger().log(Level.WARNING, "Error while disabling plugin ", e);
        }
        unregisterListener();
        unregisterTasks();
        unregisterMenus();
        File jar = assigned.getLoader().getJar();
        if(!jar.delete()){
            jar.deleteOnExit();
        }
        assigned = null;
        endSetup("Server shutdown");
    }

    public boolean enableAddon(ServerType type) {
        if (getAssigned() != null) {
            disableAddon();
        }
        if(!getPlugin().getDataFolder().exists()){
            getPlugin().getDataFolder().mkdir();
        }
        File jar = new File(getPlugin().getDataFolder(),type.getName() + ".jar");
        try{
            DownloadUtil.download(jar, type.getDownload(), getFileConnection());
        }
        catch (Exception ex){
            getLogger().log(Level.WARNING, "Error downloading addon", ex);
            return false;
        }
        AddonDescriptionFile file;
        try {
            file = DevotedAddonLoader.getPluginDescription(jar);
        } catch (InvalidDescriptionException e) {
            getLogger().log(Level.WARNING, "Error getting addon description file", e);
            return false;
        }
        DevotedAddonLoader loader;
        try{
            loader = new DevotedAddonLoader(jar, file);
        }
        catch (Exception ex){
            getLogger().log(Level.WARNING, "Failed to load addon", ex);
            return false;
        }
        DevotedAddon plugin = loader.getPlugin();
        plugin.__init__(loader);
        getLogger().log(Level.INFO, "{0} has been selected", file.getName());
        assigned = plugin;
        try {
            getAssigned().onLoad();
        } catch (Exception e) {
            getLogger().log(Level.WARNING, "Error whilst loading " + file.getName(), e);
            return false;
        }
        getLogger().log(Level.INFO, "{0} is loaded", file.getName());
        try {
            getAssigned().onEnable();
        } catch (Exception e) {
            getLogger().log(Level.WARNING, "Error whilst enabling " + file.getName(), e);
            return false;
        }
        getLogger().log(Level.INFO, "{0} is enabled", file.getName());
        return true;
    }

    public void onMessage(PacketInfo info, IPluginMessage message) {
        if (message instanceof AssignMessage) {
            getLogger().log(Level.INFO, "Received assign message");
            AssignMessage assignMessage = (AssignMessage) message;
            type = assignMessage.getWrapperType();
            id = assignMessage.getId();
            getLogger().log(Level.INFO, "ServerType: {0} ID: {1}", new Object[]{type.getName(), id});
            proxy = info.getServer();
            //Instant reply so Joinable update works
            try {
                getPacketHub().sendMessage(proxy, new AssignMessage(id, type));
            } catch (IOException e) {
                getLogger().log(Level.WARNING, "Could not send assign message", e);
                endSetup("Could not send assign message");
            }
            if(!enableAddon(type)){
                if(getAssigned() != null)disableAddon();
                endSetup("Could not setup addon");
            }
        } else if (message instanceof RankDataUpdate) {
            RankDataUpdate rankDataUpdate = (RankDataUpdate) message;
            Rank.loadRank(rankDataUpdate.getName(), rankDataUpdate.getData());
            getLogger().log(Level.INFO, "Loaded rank: " + rankDataUpdate.getName());
        } else if (message instanceof PlayerDataResponse) {
            final PlayerDataResponse dataResponse = (PlayerDataResponse) message;
            final Player player = Bukkit.getPlayer(dataResponse.getName());
            if (player != null) {
                final BukkitDevotedPlayer bukkitDevotedPlayer;
                if ((bukkitDevotedPlayer = BukkitDevotedPlayer.getObject(player.getUniqueId())) == null) {
                    getLogger().log(Level.WARNING, "Received data for a player which is not online " + dataResponse.getName());
                } else {
                    //Call event Async
                    new BukkitRunnable(){
                        @Override
                        public void run() {
                            if(player.isOnline() && bukkitDevotedPlayer.isOnline()) {
                                PlayerDataReceivedEvent event = new PlayerDataReceivedEvent(player, new PlayerData(dataResponse.getData()));
                                getPlugin().getServer().getPluginManager().callEvent(event);
                                bukkitDevotedPlayer.setData(event.getData().getRaw());
                            }
                        }
                    }.runTaskAsynchronously(getPlugin());
                }
            } else {
                DataRequestTask.setData(dataResponse.getName(), dataResponse.getData());
            }
        } else if (message instanceof PlayerDataRequest) {
            PlayerDataRequest request = (PlayerDataRequest) message;
            Player p = Bukkit.getPlayer(request.getName());
            if (p != null) {
                DevotedPlayer<Player> player = BukkitDevotedPlayer.getObject(p.getUniqueId());
                if (player != null) {
                    try {
                        getPacketHub().sendMessage(getProxy(), new PlayerDataResponse(request.getName(), player.getData().getRaw()));
                    } catch (IOException e1) {
                        getLogger().log(Level.WARNING, "Could not save playerdata for " + request.getName(), e1);
                    }
                } else {
                    getLogger().log(Level.WARNING, "BubblePlayer not found for data request " + request.getName());
                }
            } else {
                getLogger().log(Level.WARNING, "Player not found for data request " + request.getName());
            }
        } else if(message instanceof ServerListResponse){
            ServerListResponse response = (ServerListResponse)message;
            getPlugin().getServer().getPluginManager().callEvent(new ServerListUpdateEvent(response.getServertype(), response.getServerList()));
        } else {
            getLogger().log(Level.WARNING, "Unsupported message " + message.getType().getName());
        }
    }

    public XServer getProxy() {
        return proxy;
    }

    public DevotedAddon getAssigned() {
        return assigned;
    }

    public ServerType getType() {
        return type;
    }

    public int getId() {
        return id;
    }

    public XServerPlugin getXPlugin() {
        if (!getPlugin().getServer().getPluginManager().isPluginEnabled("XServer")) {
            endSetup("Could not find XServer");
        }
        return (XServerPlugin) getPlugin().getServer().getPluginManager().getPlugin("XServer");
    }

    public void runTaskLater(Runnable runnable, long l, TimeUnit timeUnit) {
        getPlugin().getServer().getScheduler().runTaskLater(getPlugin(), runnable, timeUnit.toMillis(l) / 50);
    }

    public File getReplace() {
        return file;
    }

    public String getArtifact() {
        return getPlugin().getName();
    }

    public int getVersion() {
        return VERSION;
    }

    public void update(Runnable update) {
        if (getAssigned() != null) {
            runTaskLater(update, getAssigned().finishUp(), TimeUnit.SECONDS);
        } else {
            runTaskLater(update, 0L, TimeUnit.SECONDS);
        }
    }

    public void sendPlayer(Player p, String server) {
        try {
            getPacketHub().sendMessage(getProxy(), new PlayerMoveRequest(p.getName(), server));
        } catch (IOException e) {
            getLogger().log(Level.WARNING, "Could not move player ", e);
        }
    }

    public void sendPlayer(Player p, ServerType server) {
        try {
            getPacketHub().sendMessage(getProxy(), new PlayerMoveTypeRequest(p.getName(), server));
        } catch (IOException e) {
            getLogger().log(Level.SEVERE, "Could not move player ", e);
        }
    }

    public boolean bungee() {
        return false;
    }

    public void onConnect(PacketInfo info) {
    }

    public void onDisconnect(PacketInfo info) {
    }

    public void updateTaskBefore() {
        getPlugman().unload(getPlugin());
    }

    public void updateTaskAfter() {
        getPlugman().load(file);
    }

    public BukkitPlugman getPlugman() {
        return plugman;
    }

    public BukkitDevotedPlayer getDevotedPlayer(UUID uuid) {
        if(BukkitDevotedPlayer.getPlayerObjectMap().containsKey(uuid)){
            return BukkitDevotedPlayer.getObject(uuid);
        }
        throw new UnsupportedOperationException("Server side cannot get offline devotedplayers");
    }
}
