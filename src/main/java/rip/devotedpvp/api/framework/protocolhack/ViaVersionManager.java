package rip.devotedpvp.api.framework.protocolhack;

import rip.devotedpvp.api.framework.DevotedNetwork;
import rip.devotedpvp.api.framework.util.plugindownloader.AbstractPluginDownloader;

import java.io.File;


public class ViaVersionManager extends AbstractPluginDownloader{
    public ViaVersionManager() {
        super(DevotedNetwork.getInstance(), "ViaVersion.jar", new File(DevotedNetwork.getInstance().getPlugin().getDataFolder(),"ViaVersion.jar"));
    }

    public Unsafe unsafe(){
        return new Unsafe() {
            public ViaHook getHook() {
                if(getPlugin() != null && getPlugin().isEnabled())return new ViaHook(getPlugin());
                throw new IllegalArgumentException("Plugin is not enabled");
            }
        };
    }

    interface Unsafe{
        ViaHook getHook();
    }
}
