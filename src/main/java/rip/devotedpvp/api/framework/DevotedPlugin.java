package rip.devotedpvp.api.framework;

import rip.devotedpvp.api.framework.util.mc.world.VoidWorldGenerator;
import rip.devotedpvp.api.global.file.DownloadUtil;
import org.bukkit.generator.ChunkGenerator;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.util.logging.Level;

public class DevotedPlugin extends JavaPlugin {

    private DevotedNetwork network;

    public DevotedPlugin() {
        super();
        network = new DevotedNetwork(this);
    }

    public void onLoad() {
        network.onLoad();
    }

    public void onEnable() {
        network.onEnable();
    }

    public void onDisable() {
        try {
            DownloadUtil.download(getFile(), "DevotedFramework.jar", network.getFileConnection());
        } catch (Exception e) {
            getLogger().log(Level.WARNING, "Could not update ", e);
        }
        network.onDisable();
    }

    public ChunkGenerator getDefaultWorldGenerator(String s1, String s2) {
        return VoidWorldGenerator.getGenerator();
    }

    public File getFile() {
        return super.getFile();
    }

}
