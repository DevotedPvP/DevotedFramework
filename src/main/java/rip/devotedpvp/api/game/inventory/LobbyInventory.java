package rip.devotedpvp.api.game.inventory;

import rip.devotedpvp.api.framework.DevotedNetwork;
import rip.devotedpvp.api.framework.util.mc.menu.BuyInventory;
import rip.devotedpvp.api.global.type.ServerType;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class LobbyInventory extends BuyInventory {
    public LobbyInventory() {
        super(ChatColor.GOLD + "Teleport to lobby", "Teleport me to lobby", "Cancel");
    }

    public void onCancel(Player p) {
        p.closeInventory();
    }

    public void onAllow(Player p) {
        p.closeInventory();
        DevotedNetwork.getInstance().sendPlayer(p, ServerType.getType("Lobby"));
    }
}
