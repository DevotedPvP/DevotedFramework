package rip.devotedpvp.api.game.scoreboard;

import rip.devotedpvp.api.framework.DevotedNetwork;
import rip.devotedpvp.api.framework.util.mc.scoreboard.board.SingleBoard;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;

import java.util.*;


public class GameBoard extends SingleBoard {
    public static Collection<GameBoard> getBoards() {
        return boardMap.values();
    }

    public static void setBoard(Player p,GameBoard board){
        boardMap.put(p.getUniqueId(),board);
    }

    public static void removeBoard(Player p){
        boardMap.remove(p.getUniqueId());
    }

    public static GameBoard getBoard(Player p) {
        return boardMap.get(p.getUniqueId());
    }

    private static Map<DisplaySlot, String> displayname = Collections.singletonMap(DisplaySlot.SIDEBAR, DevotedNetwork.getPrefix());
    //CUSTOM LOGGING
    private static Map<UUID, GameBoard> boardMap = new HashMap<>();

    public GameBoard(Player p) {
        super(p, displayname);
    }
}
