package rip.devotedpvp.api.game.kit;

import rip.devotedpvp.api.framework.DevotedNetwork;
import rip.devotedpvp.api.framework.player.BukkitDevotedPlayer;
import rip.devotedpvp.api.framework.util.mc.menu.BuyInventory;
import rip.devotedpvp.api.game.GameAPI;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;


public class KitLevelUpInventory extends BuyInventory {
    private static final Sound BUYKIT = Sound.LEVEL_UP, CANCELBUY = Sound.BLAZE_HIT;
    private int cost;
    private Kit k;
    private boolean cancelled = false;
    private int level;

    public KitLevelUpInventory(Kit k, int cost, int level) {
        super(ChatColor.GOLD + k.getNameClear() + " Lv" + String.valueOf(level - 1) + " -> Lv" + String.valueOf(level) + " " + ChatColor.RED + String.valueOf(cost) + "T");
        this.k = k;
        this.cost = cost;
        this.level = level;
        GameAPI.getInstance().registerListener(new Listener() {
            @EventHandler
            public void onInventoryClose(InventoryCloseEvent e) {
                if (e.getInventory() == getInventory()) {
                    cancelled = true;
                    DevotedNetwork.getInstance().unregisterMenu(KitLevelUpInventory.this);
                    HandlerList.unregisterAll(this);
                }
            }
        });
    }

    public Kit getKit() {
        return k;
    }

    public int getCost() {
        return cost;
    }

    public int getLevel() {
        return level;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public void onCancel(Player player) {
        player.closeInventory();
        KitSelection.openMenu(player);
        player.playSound(player.getLocation().getBlock().getLocation(), CANCELBUY, 1f, 1f);
    }

    public void onAllow(Player player) {
        BukkitDevotedPlayer devotedPlayer = BukkitDevotedPlayer.getObject(player.getUniqueId());
        if(devotedPlayer.canAfford(getCost())) {
            devotedPlayer.setTokens(devotedPlayer.getTokens() - cost);
            getKit().level(devotedPlayer, getLevel());
            KitSelection.getSelection(player).update();
            player.closeInventory();
            KitSelection.openMenu(player);
            player.playSound(player.getLocation().getBlock().getLocation(), BUYKIT, 1f, 1f);
        }
        else player.sendMessage(DevotedNetwork.getPrefix() + "You can't afford this");
    }
}
