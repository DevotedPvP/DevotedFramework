package rip.devotedpvp.api.game.kit;

import rip.devotedpvp.api.framework.DevotedNetwork;
import rip.devotedpvp.api.framework.player.BukkitDevotedPlayer;
import rip.devotedpvp.api.framework.util.mc.menu.BuyInventory;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

public class KitBuyInventory extends BuyInventory {
    private static final Sound BUYKIT = Sound.LEVEL_UP, CANCELBUY = Sound.BLAZE_HIT;

    private Kit kit;

    public KitBuyInventory(Kit kit) {
        super(ChatColor.DARK_GRAY + "Buy " + ChatColor.GOLD + kit.getNameClear() + ChatColor.DARK_GRAY + " T" +
                ChatColor.GREEN + String.valueOf(kit.getPrice()));
        this.kit = kit;
    }

    public void onCancel(Player player) {
        player.closeInventory();
        KitSelection.openMenu(player);
        player.playSound(player.getLocation().getBlock().getLocation(), CANCELBUY, 1f, 1f);
    }

    public void onAllow(Player player) {
        BukkitDevotedPlayer devotedPlayer = BukkitDevotedPlayer.getObject(player.getUniqueId());
        if(devotedPlayer.canAfford(getKit().getPrice())) {
            devotedPlayer.setTokens(devotedPlayer.getTokens() - getKit().getPrice());
            getKit().buy(devotedPlayer);
            KitSelection.getSelection(player).update();
            player.closeInventory();
            KitSelection.openMenu(player);
            player.playSound(player.getLocation().getBlock().getLocation(), BUYKIT, 1f, 1f);
        }
        else player.sendMessage(DevotedNetwork.getPrefix() + "You can't afford this");
    }

    public Kit getKit() {
        return kit;
    }
}
