package rip.devotedpvp.api.game.kit;

import rip.devotedpvp.api.framework.player.BukkitDevotedPlayer;
import rip.devotedpvp.api.framework.util.mc.chat.ChatColorAppend;
import rip.devotedpvp.api.game.GameAPI;
import org.apache.commons.lang.Validate;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public class Kit {
    private final String name;
    private Material display;
    private List<ItemStack[]> inventorypreset;
    private String[] description;
    private int price;
    private KitBuyInventory buyInventory;

    public Kit(Material display, List<ItemStack[]> inventorypreset, String name, String[] description, int price) {
        Validate.notEmpty(inventorypreset);
        Validate.noNullElements(inventorypreset);
        this.display = display;
        this.inventorypreset = inventorypreset;
        this.name = name;
        this.description = description;
        this.price = price;
        buyInventory = new KitBuyInventory(this);
    }

    public int getMaxlevel() {
        return inventorypreset.size();
    }

    public boolean isOwned(BukkitDevotedPlayer player) {
        return getLevel(player) > 0;
    }

    public int getLevel(BukkitDevotedPlayer player) {
        int level = player.getKit(GameAPI.getInstance().getName(), getNameClear());
        if(level > 0){
            return level;
        }
        return GameAPI.getInstance().getDefaultKit() == this ? 1 : 0;
    }

    public int getLevelUpcost(BukkitDevotedPlayer player) {
        int level = getLevel(player);
        if (level >= getMaxlevel()) {
            throw new IllegalArgumentException("Cannot level up");
        }
        return price + ((price * (level + 1)) / (getMaxlevel() - level));
    }

    public void apply(BukkitDevotedPlayer p) {
        Player bukkitPlayer = p.getPlayer();
        int level = getLevel(p);
        bukkitPlayer.getInventory().clear();
        bukkitPlayer.getInventory().setArmorContents(new ItemStack[4]);
        bukkitPlayer.getInventory().setContents(getInventorypreset(level));
    }

    public void buy(BukkitDevotedPlayer player) {
        level(player, 1);
    }

    public void level(BukkitDevotedPlayer player, int level) {
        player.setKit(GameAPI.getInstance().getName(), getNameClear(), level);
    }

    public KitBuyInventory getBuyInventory() {
        return buyInventory;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public ItemStack[] getInventorypreset(int level) {
        return inventorypreset.get(level-1).clone();
    }

    public Material getDisplay() {
        return display;
    }

    public String getName() {
        return name;
    }

    public String getNameClear() {
        return ChatColorAppend.wipe(name);
    }

    public String[] getDescription() {
        return description;
    }

    public void setDescription(String[] description) {
        this.description = description;
    }
}
